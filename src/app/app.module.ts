import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ResultsComponent } from './results/results.component';
import { BowlingComponent } from './bowling/bowling.component';


@NgModule({
  declarations: [
    AppComponent,
    ResultsComponent,
    BowlingComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
