import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bowling',
  templateUrl: './bowling.component.html',
  styleUrls: ['./bowling.component.css']
})
export class BowlingComponent implements OnInit {
  results = [];
  fileContent: string;

  constructor() { }

  ngOnInit() {

  }

  getFileContent( event ){ // wybrano plik z komputera zawierający dane z gry
    let oReader = new FileReader();
    oReader.readAsText( event.target.files[0] );
    oReader.onload = function(){
      this.fileContent = oReader.result;
      this.getResults();
    }.bind(this);
  } 

  getResults(){ // pobieranie danych z pliku
    let lines = this.fileContent.match( /[^\r\n]+/g ),
    i = 0,
    lastName = null;
    this.results = [];
        
    while( i < lines.length ){ // tworzenie tablicy obiektów z danymi: zawodnik, punkty
      let line = lines[i].trim();
      if( /\S/.test( line ) ){ // same białe znaki się nie liczą
        if( line[0].match( /\d/ ) ){ // punkty
          this.results.push({
            name: ( lastName != null ) ? lastName : '!John Doe',
            points: line.split( ',' )
          });
          lastName == null;
        }
        else{ // nazwa
          lastName = line;
        }
      }
      i++;
    }
    
    if( this.results.length > 0 ){
      for( let i = 0; i < this.results.length; i++){
        this.checkPointsValid( i ); // validacja punktów
        this.countRoundsPoints( i );
      }
    }

  }

  countRoundsPoints( playerId ){
    let summary = 0, // suma pkt dla każdego zawodnika
        rounds = [], // dane dla poszczególnych rund
        points = this.results[playerId].points;

    for( let r = 1; r <= 10; r++ ){ // rundy liczone od 1 do 10
      let shot1 = Number.parseInt( points[(r*2)-2], 10 );
      let shot2 = Number.parseInt( points[(r*2)-1], 10 );
      rounds[r] = { points: shot1 + shot2, bonus: false }; // suma pkt dla rundy bez bonusu za strike czy spare

      // sprawdzenie bonusów
      if( shot1 == 10 ){ // wszystkie zbite w jednym rzucie
        rounds[r].bonus = 'strike';
      }
      else if( rounds[r].points == 10 ){ // wszystkie zbite w rundzie
        rounds[r].bonus = 'spare';
      }

      if( r > 1 ){ // bonusy do punktów za poprzednie strzały
        // strike może być tylko w shot1, 10 zbitych w shot2 to już spare
        if( rounds[r-1].bonus == 'strike' ){ // jeśli w poprzedniej rundzie był strike
          rounds[r-1].points += shot1; // bonus - punkty z tego rzutu dodajemy do wyniku za tą poprzednią rundę (w której był strike)
          if( rounds[r].bonus != 'strike' || r == 10 ){ // jeśli w tej rundzie nie było strike to dodaję od razu shot2 (strike to bonus za 2 strzały w przód)
            rounds[r-1].points += shot2;
          } // jeśli był strike to drugi bonus za poprzedni strike będzie doliczany do poprzedniej rundy w kolejnym rzucie
          
          if( r > 2 && rounds[r-2].bonus == 'strike' ){ // i jeśli w jeszcze poprzedniej (przed-przed ;-) rundzie był strike
            rounds[r-2].points += shot1; // to doliczam shot1 do wyniku tamtej rundy
          }

        }
        else if( rounds[r-1].bonus == 'spare' ){ // jeśli w poprzedniej rundzie był spare
          // wtedy nawet gdyby wcześniej był strike, to już doliczony został bonus za 2 rzuty
          rounds[r-1].points += shot1; // bonus - punkty z tego rzutu dodaję do wyniku za porzednią runde (tą ze spare )
        }
        
        if( r == 10 && rounds[r].bonus !== false ){ // ostatnia runda STRIKE lub SPARE pozwala na oddanie trzeciego strzału
          let shot3 = Number.parseInt( points[(r*2)], 10 );
          rounds[r].points += shot3; // 3 strzał dla tej rundy
        }
      }
      
    }

    rounds.forEach(element => { // sumujemy wyniki z poszczególnych rund (z uwzględnionymi już bonusami)
      summary += element.points;
    });

    this.results[playerId].summary = summary;
    // this.results[playerId].rounds = rounds; // przyszłość
  }

  checkPointsValid( playerId ){
    let points = this.results[playerId].points;
    for( let i =0; i < 21; i++ ) {
      let shot = Number.parseInt( points[i], 10 );
      if( isNaN( shot ) ){
        shot = 0;
      }
      console.log(shot);
      
      this.results[playerId].points[i] = shot;
    }
    
  }

}
