import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  roundsIds = Array.apply(null, {length: 21}).map(function(value, index){
    return (index+1);
  });

  @Input() public results;
  constructor() { }

  ngOnInit() {
  }

}